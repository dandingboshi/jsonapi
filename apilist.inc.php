<?php
/*
 * 应用中心主页：http://addon.discuz.com/?@ailab
 * 人工智能实验室：Discuz!应用中心十大优秀开发者！
 * 插件定制 联系QQ594941227
 * From www.ailab.cn
 */

if(!defined('IN_DISCUZ') || !defined('IN_ADMINCP')) {
	exit('Access Denied');
}
require_once DISCUZ_ROOT.'./source/plugin/jsonapi/functions.php';
jsonApiLoad();
$op=$_GET['op'];
if($op=='close'){
	$api=trim($_GET['api']);
	if(in_array($api,$_JSONAPI['class'])) $_class=new $api;
	else{
		cpmsg('对不起，您指定的API不存在！');
	}
	$_class->_config['status']=0;
	save_syscache($_class::$_cachekey,$_class->_config);
	cpmsg('恭喜您，该API已关闭！','action=plugins&operation=config&do='.$pluginid.'&identifier=jsonapi&pmod=apilist', 'succeed');	
}elseif($op=='open'){
	$api=trim($_GET['api']);
	if(in_array($api,$_JSONAPI['class'])) $_class=new $api;
	else{
		cpmsg('对不起，您指定的API不存在！');
	}
	$_class->_config['status']=1;
	save_syscache($_class::$_cachekey,$_class->_config);
	cpmsg('恭喜您，该API已开启！','action=plugins&operation=config&do='.$pluginid.'&identifier=jsonapi&pmod=apilist', 'succeed');
}elseif($op=='edit'){
	$api=trim($_GET['api']);
	if(in_array($api,$_JSONAPI['class'])) $_class=new $api;
	else{
		cpmsg('对不起，您指定的API不存在！');
	}
	if(submitcheck('editsubmit')){
		$_class->_config['return']=$_POST['return'];
		$_class->_config['filter']=$_POST['filter'];
		$_class->_config['orderby']=$_POST['orderby'];
		save_syscache($_class::$_cachekey,$_class->_config);
		cpmsg('字段编辑成功！','action=plugins&operation=config&do='.$pluginid.'&identifier=jsonapi&pmod=apilist', 'succeed');
	}else{
		showformheader("plugins&operation=config&do=$pluginid&identifier=jsonapi&pmod=apilist&op=edit&api=".$api);
		showtableheader($_class::$_api.'接口字段管理', 'nobottom');	
		showsubtitle(array('字段','字段名称','可返回','可筛选','可排序'));
		foreach($_class::$_keylist as $key=>$keyname){
			showtablerow('',array(), array(
				$key,
				$keyname,
				'<input class="checkbox" type="checkbox" name="return['.$key.']" value="1" '.($_class->_config['return'][$key]? 'checked':'').'>',
				'<input class="checkbox" type="checkbox" name="filter['.$key.']" value="1" '.($_class->_config['filter'][$key]? 'checked':'').'>',
				'<input class="checkbox" type="checkbox" name="orderby['.$key.']" value="1" '.($_class->_config['orderby'][$key]? 'checked':'').'>',
			));
		}
		showsubmit('editsubmit');
		showtablefooter();
		showformfooter();
	}
}else{//列表
	//默认列表;	
	showtableheader('API接口管理', 'nobottom');
	showsubtitle(array('API','API名称','API介绍','API状态','编辑字段','启用/关闭'));
	foreach($_JSONAPI['class'] as $k=>$C){
		loadcache($C::$_cachekey);
		showtablerow('',array(), array(
			$C::$_api,
			$C::$_name,
			$C::$_info,
			$_G['cache'][$C::$_cachekey]['status']? '<font color="green">已启用</font>':'<font color="red">未启用</font>',
			'<a href="###" onclick="location.href=\''.ADMINSCRIPT.'?action=plugins&operation=config&do='.$pluginid.'&identifier=jsonapi&pmod=apilist&op=edit&api='.$C.'\'">编辑</a>',
			$_G['cache'][$C::$_cachekey]['status']? '<a href="###" onclick="location.href=\''.ADMINSCRIPT.'?action=plugins&operation=config&do='.$pluginid.'&identifier=jsonapi&pmod=apilist&op=close&api='.$C.'\'">关闭</a>':'<a href="###" onclick="location.href=\''.ADMINSCRIPT.'?action=plugins&operation=config&do='.$pluginid.'&identifier=jsonapi&pmod=apilist&op=open&api='.$C.'\'">启用</a>',
		));
	}
	showtablefooter();

}

?>